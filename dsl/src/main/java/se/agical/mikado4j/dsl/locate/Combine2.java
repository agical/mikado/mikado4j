package se.agical.mikado4j.dsl.locate;

public interface Combine2<C1, C2, CMB> {

	Many<CMB> combine(C1 c1, C2 c2);

}
