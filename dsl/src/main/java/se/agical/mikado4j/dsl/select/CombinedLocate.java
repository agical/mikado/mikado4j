package se.agical.mikado4j.dsl.select;

import java.util.List;

public interface CombinedLocate<CR, C, NC> {
	List<NC> combinedLocate(C current, CR root);
}
