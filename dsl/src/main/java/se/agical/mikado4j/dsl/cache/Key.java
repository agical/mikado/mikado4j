package se.agical.mikado4j.dsl.cache;

import java.util.Objects;

public class Key<T> {
	private final Object uniqueKey;

	public Key(final Object uniqueKey) {
		this.uniqueKey = uniqueKey;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Key<?> other = (Key<?>) obj;
		if (!Objects.equals(uniqueKey, other.uniqueKey)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hash(uniqueKey);
	}

	@Override
	public String toString() {
		return uniqueKey.toString();
	}

}
