package se.agical.mikado4j.dsl.resolve;

import java.util.Optional;

import se.agical.mikado4j.dsl.locate.Locator;

public class ResolutionBuilder<RC, CC> {

	final Locator<RC, CC> locatorForDetails;

	public ResolutionBuilder(Locator<RC, CC> locator) {
		locatorForDetails = locator;

	}

	public WithName name(String name) {
		return new WithName(name);
	}

	public abstract class DefineSolution implements FinishResolution<RC> {

		public abstract WithSolution solveWith(Solution<CC> solution);

	}

	public class WithName extends DefineSolution {
		private String name;

		public WithName(String name) {
			this.name = name;
		}

		public WithDescription shortDescription(Describe<CC> describe) {
			return new WithDescription(name, describe);
		}

		@Override
		public Resolution<RC> finish() {
			return new Resolution<RC>(name, Optional.empty(), Optional.empty());
		}

		@Override
		public WithSolution solveWith(Solution<CC> solution) {
			return new WithSolution(name, Optional.empty(), solution);
		}

	}

	public class WithDescription extends DefineSolution {

		private Describe<CC> describe;
		private String name;

		public WithDescription(String name, Describe<CC> describe) {
			this.name = name;
			this.describe = describe;
		}

		@Override
		public Resolution<RC> finish() {
			return new Resolution<CC>(name, Optional.of(describe), Optional.empty()).viaLocator(locatorForDetails);
		}

		@Override
		public WithSolution solveWith(Solution<CC> solution) {
			return new WithSolution(name, Optional.of(describe), solution);
		}

	}

	public class WithSolution implements FinishResolution<RC> {

		private final String name;
		private final Optional<Describe<CC>> maybeDescription;
		private final Solution<CC> solution;

		public WithSolution(String name, Optional<Describe<CC>> maybeDescription, Solution<CC> solution) {
			this.name = name;
			this.maybeDescription = maybeDescription;
			this.solution = solution;
		}

		@Override
		public Resolution<RC> finish() {
			return new Resolution<>(name, maybeDescription, Optional.of(solution)).viaLocator(locatorForDetails);
		}
	}

}
