package se.agical.mikado4j.dsl.resolve;

import java.util.Optional;
import java.util.stream.Stream;

import se.agical.mikado4j.dsl.locate.Locator;

public class Resolution<CC> {

	private final String name;
	private final Optional<Describe<CC>> maybeDescribe;
	public final Optional<Solution<CC>> maybeSolution;

	public Resolution(String name, Optional<Describe<CC>> maybeDescribe, Optional<Solution<CC>> maybeSolution) {
		this.name = name;
		this.maybeDescribe = maybeDescribe;
		this.maybeSolution = maybeSolution;
	}

	public <NC> Resolution<NC> viaLocator(Locator<NC, CC> locator) {
		return new Resolution<NC>(name,
				maybeDescribe.map(d -> d.viaLocator(locator)),
				maybeSolution.map(s -> s.viaLocator(locator)));
	}

	public void previewResolveShort(String indent, CC context, char status) {
		System.out.printf("%s[%s] %s\n", indent, status, name);
	}

	public void previewResolveLong(String indent, CC context, char status) {
		maybeDescribe
				.stream()
				.flatMap(describe -> describe.describe(context).stream())
				.flatMap(s -> {
					var multiline = s.split("\n", -1);
					for (int i = 0; i < multiline.length; i++) {
						String line = multiline[i];
						multiline[i] = String.format("%s %s", i == 0 ? String.format("[%s]", status) : "   ", line);
					}
					return Stream.of(multiline);
				})
				.map(s -> String.format("%s%s", indent, s))
				.forEach(System.out::println);
	}

}
