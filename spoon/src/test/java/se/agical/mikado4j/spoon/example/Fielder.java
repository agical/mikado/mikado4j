package se.agical.mikado4j.spoon.example;

import java.util.stream.Stream;

import se.agical.mikado4j.spoon.example.subPackage.ASubClass;
import se.agical.mikado4j.spoon.example.subPackage.ASubSubClass;

public class Fielder {

	private static String privateStaticString = "privatStaticString";
	protected static String protectedStaticString = "protectedStaticString";
	public static String publicStaticString = "publicStaticString";
	static String defaultStaticString = "defaultStaticString";

	private final static String privateFinalStaticString = "privatFinalStaticString";
	protected final static String protectedFinalStaticString = "protectedFinalStaticString";
	public static final String publicFinalStaticString = "publicFinalStaticString";
	static final String defaultFinalStaticString = "defaultFinalStaticString";

	private final String privateString = "privatString";
	protected String protectedString = "protectedString";
	public String publicString = "publicString";
	String defaultString = "defaultString";

	AClass aClassField = new AClass();
	ASubClass aSubClassField = new ASubClass();
	ASubSubClass aSubSubClassField = new ASubSubClass();

	protected void noFieldWarnings() {
		Stream.of(privateString + privateStaticString + privateFinalStaticString);
	}

}
