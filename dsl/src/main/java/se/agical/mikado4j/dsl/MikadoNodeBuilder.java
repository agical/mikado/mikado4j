package se.agical.mikado4j.dsl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import se.agical.mikado4j.dsl.locate.Locator;
import se.agical.mikado4j.dsl.resolve.DoneChecker;
import se.agical.mikado4j.dsl.resolve.Resolution;
import se.agical.mikado4j.dsl.select.Select;

public class MikadoNodeBuilder<RC, CC> implements Select<RC, CC> {

	private final List<MikadoNode<RC>> prereqs;
	private final List<Resolution<RC>> resolutions;
	private final String name;
	private final Locator<RC, CC> locator;
	private final DoneChecker<CC> doneChecker;

	public static <C> MikadoNodeBuilder<C, C> create(String name) {
		return new MikadoNodeBuilder<>(name, Locator.wrap(c -> () -> Stream.of(c)), new ArrayList<MikadoNode<C>>(),
				new ArrayList<>());
	}

	public MikadoNodeBuilder(String name, Locator<RC, CC> locator, List<MikadoNode<RC>> prereqs,
			List<Resolution<RC>> resolutions) {
		this(name, locator, prereqs, resolutions, x -> false);
	}

	public MikadoNodeBuilder(String name, Locator<RC, CC> locator, List<MikadoNode<RC>> prereqs,
			List<Resolution<RC>> resolutions, DoneChecker<CC> doneChecker) {
		this.name = name;
		this.locator = locator;
		this.prereqs = prereqs;
		this.resolutions = resolutions;
		this.doneChecker = doneChecker;
	}

	@SuppressWarnings("unchecked")
	public MikadoNode<RC> finish() {
		return new MikadoNode<RC>(
				name,
				new CardinalityResolver<>(locator),
				resolutions.stream().toArray(Resolution[]::new),
				new DoneResolver<>(doneChecker).viaLocator(locator),
				prereqs.stream().toArray(MikadoNode[]::new));
	}

	@Override
	public Locator<RC, CC> locator() {
		return locator;
	}

	@Override
	public MikadoNodeBuilder<RC, CC> prereq(MikadoNode<RC> prereq) {
		prereqs.add(prereq);
		return this;
	};

	@Override
	public <NC> Select<RC, NC> updateSelection(Locator<RC, NC> newSelection) {
		return new MikadoNodeBuilder<RC, NC>(name, newSelection, prereqs, resolutions);
	}

	@Override
	public void addResolution(Resolution<RC> resolution) {
		resolutions.add(resolution);
	}

	@Override
	public Prereqs<RC, CC> updateDoneChecker(DoneChecker<CC> doneChecker) {
		return new MikadoNodeBuilder<RC, CC>(name, locator, prereqs, resolutions, doneChecker);
	}
}
