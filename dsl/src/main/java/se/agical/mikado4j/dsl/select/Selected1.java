package se.agical.mikado4j.dsl.select;

import se.agical.mikado4j.dsl.MikadoNode;
import se.agical.mikado4j.dsl.Prereqs;
import se.agical.mikado4j.dsl.locate.Locator;
import se.agical.mikado4j.dsl.resolve.DoneChecker;
import se.agical.mikado4j.dsl.resolve.Resolution;
import se.agical.mikado4j.dsl.resolve.Resolutions;

public class Selected1<RC, C1> implements Resolutions<RC, C1> {

	private Select<RC, C1> currentSelection;

	public Selected1(Select<RC, C1> currentSelection) {
		this.currentSelection = currentSelection;
	}

	public <C2> Selected2<RC, C1, C2> select(Locator<C1, C2> resolution) {
		return new Selected2<>(currentSelection, currentSelection.locator().andThen(resolution));
	}

	public <C2> Selected2<RC, C1, C2> selectAbsolute(Locator<RC, C2> resolution) {
		return new Selected2<>(currentSelection, resolution);
	}

	@Override
	public MikadoNode<RC> finish() {
		return currentSelection.finish();
	}

	@Override
	public Locator<RC, C1> locator() {
		return currentSelection.locator();
	}

	@Override
	public Prereqs<RC, C1> prereq(MikadoNode<RC> prereq) {
		return currentSelection.prereq(prereq);
	}

	@Override
	public void addResolution(Resolution<RC> reslution) {
		currentSelection.addResolution(reslution);
	}

	@Override
	public Prereqs<RC, C1> updateDoneChecker(DoneChecker<C1> doneChecker) {
		return currentSelection.updateDoneChecker(doneChecker);
	}

}
