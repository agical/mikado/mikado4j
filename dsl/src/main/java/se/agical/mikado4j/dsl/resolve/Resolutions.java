package se.agical.mikado4j.dsl.resolve;

public interface Resolutions<RC, CC> extends DefineDone<RC, CC> {
	void addResolution(Resolution<RC> reslution);

	default Resolutions<RC, CC> resolve(ResolutionDefiner<RC, CC> rd) {
		addResolution(rd.build(new ResolutionBuilder<RC, CC>(locator())).finish());
		return this;
	}

}
