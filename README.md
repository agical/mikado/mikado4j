## Mikado4J

Mikado4J is an automated [mikado](http://mikadomethod.info) solver written in Java. It was developed to be coupled with [spoon](https://spoon.gforge.inria.fr/index.html) to do automated code changes as parts of large refactorings, but we have found that it can also solve other problems that lend themselves to breaking down as a mikado graph. It provides a [DSL](https://en.wikipedia.org/wiki/Domain-specific_language)to declare goals and preconditions, as well as analisys and automated solving of said goals and preconditions.

### What it does

The DSL aims to support programmers in all stages of problemsolving, from identifying preconditions to solving them. Roughly a precondition goes through some of the following stages in order:

1. Identify - a problem is identified and registered as a precondition
2. Analyze - a number of data points are checked to determine all occurences of the problem 
3. Define done - check if a precondition is done or not
4. Solve - create an automation that solves an analyzed problem

You typically do not do all steps in one go, usually you begin with just identifying preconditions and then iteratively refine your graph until it, potentially, solves itself.

Your work can be visalized, if you so desire, by automatically pushing the state of your graph to [Mikado Tool](
https://tool.mikadomethod.info/). It is a great way to share your progress and bring others on board to help out.

### What it looks like

Here is a simple example of a graph. It does not analyse or make changes to code, it works with a list of integers as it's _context_. While this may be somewhat disapointing it communicates an important point: your problem does not have to be code related to lend itself to the DSL, though to be honest, we are not sure if it is a good idea to use it for other things. However, as an example that is intended to communicate how the DSL works it is convenient to leave the complexities of AST analisys and code transformations aside -- for now.

```java
MikadoGraph<List<Integer>> mikadoGraph = MikadoGraphBuilder
    .<List<Integer>>goal("No even numbers", b -> b
	.select(context -> context.stream()
        	.filter(n -> n % 2 == 0)
        	.collect(Collectors.toList()))
	.prereq("first this needs to be done"));
```

In this simple example two of the stages are adressed: _identification_ and _analisys_. The _identification_ is simply coming up with a short description of the identified goal or precontition. In this case `"No even numbers"`, and `"first this needs to be done"`, wher the latter is a precondition for the former.
The _analisys_ part is finding all the even numbers in the provided context. This is done with the `select` method in which we use an anonymous lambda to return all even numbers that need to be adressed.

Lets move on to stage 3, _define done_ where we define what is required to mark the current prerequsite as done. Let's add that:

```java
MikadoGraph<List<Integer>> mikadoGraph = MikadoGraphBuilder
    .<List<Integer>>goal("No even numbers", b -> b
	.select(context -> context.stream()
        	.filter(n -> n % 2 == 0)
        	.collect(Collectors.toList())
    	.doneWhenNoneLeft())
	.prereq("first this needs to be done"));
```

We just added the standard `doneWhenNoneLeft` method to indicate that this node should be marked as done automatically when the select does not find anymore even numbers, this is a common enough case to merit a conveniencemethod. For more advanced checking you can call `updateDoneChecker` with a custom lamda that checks the context if the node is done. We can also mark the node explicitly as `notDone` or `done` when it is hard or inefficient to automate done-detection.

The missing piece now is to add a resolution that would make this node done. Lets add that aswell:

```java
MikadoGraph<List<Integer>> mikadoGraph = MikadoGraphBuilder
    .<List<Integer>>goal("No even numbers", b -> b
	.select(context -> context.stream()
        	.filter(n -> n % 2 == 0)
        	.collect(Collectors.toList())
    	.resolve(r -> r.name("divide even by two"))
    	.doneWhenNoneLeft())
	.prereq("first this needs to be done"));
```
Again, solutions can be left as identified and even analyzed before a solution is implemented. Above we simply identified that dividing even numbers by two will eventually make them odd given enough iterations. But we have not implemented the actual solution.

```java
.resolve(r -> r
        .name("divide even by two")
        .shortDescription(i -> Arrays.asList(String.format("divide by two %d/2=%d", i, i / 2))))
```

We have still not implemented an automated solution but we have described what a human should do to solve it. Sometimes this is enough.

_*TODO* update solution to return the change to be done for immutable things like integers_

### How it works
