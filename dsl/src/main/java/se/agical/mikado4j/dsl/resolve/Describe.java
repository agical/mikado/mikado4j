package se.agical.mikado4j.dsl.resolve;

import java.util.List;
import java.util.stream.Collectors;

import se.agical.mikado4j.dsl.locate.Locator;

@FunctionalInterface
public interface Describe<CC> {
	List<String> describe(CC context);

	default <RC> Describe<RC> viaLocator(Locator<RC, CC> locatorForDetails) {
		return c -> locatorForDetails
				.locate(c)
				.stream()
				.flatMap(cc -> describe(cc).stream())
				.collect(Collectors.toList());
	};
}
