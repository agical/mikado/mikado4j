package se.agical.mikado4j.spoon.example;

public class Caller {

	public Callee callee = new Callee();

	public String referencer = callee.referencedField;
	public Fielder fielder = new Fielder();
	public AClass referencedClass = fielder.aClassField;

	public void method() {
		callee.hello();
	}
}
