package se.agical.mikado4j.dsl.select;

import se.agical.mikado4j.dsl.locate.Combine2;
import se.agical.mikado4j.dsl.locate.Locator;

public class Selected2<RC, C1, C2> {
	private Select<RC, C1> currentSelection;
	private Locator<RC, C2> locator;

	Selected2(Select<RC, C1> currentSelection, Locator<RC, C2> locator) {
		this.currentSelection = currentSelection;
		this.locator = locator;
	}

	public <CMB> Select<RC, CMB> combine(Combine2<C1, C2, CMB> combine2) {
		return currentSelection.updateSelection(
				(RC rc) -> currentSelection
						.locator()
						.andThen(
								c1 -> locator
										.andThen(c2 -> combine2.combine(c1, c2))
										.locate(rc))
						.locate(rc));
	}
}
