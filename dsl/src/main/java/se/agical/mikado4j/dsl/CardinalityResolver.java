package se.agical.mikado4j.dsl;

import se.agical.mikado4j.dsl.locate.Locator;

public class CardinalityResolver<RC> {
	private final Locator<RC, ?> locator;

	public CardinalityResolver(Locator<RC, ?> locator) {
		super();
		this.locator = locator;
	}

	public int cardinality(RC context) {
		return (int) locator.locate(context).stream().count();
	}

	public <NC> CardinalityResolver<NC> viaLocator(Locator<NC, RC> fromLocator) {
		return new CardinalityResolver<>(fromLocator.andThen(locator));
	}

}
