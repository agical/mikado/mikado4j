package se.agical.mikado4j.dsl.json;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.google.gson.Gson;

class NodeTest {

	@Test
	void nodeWithoutPrereqsSerializesCorrectly() {
		Node goal = Node.goal("A goal");

		String json = new Gson().toJson(goal);
		assertEquals("{\"desc\":\"A goal\",\"cardinality\":1,\"done\":false,\"reqs\":[]}", json);
	}

}
