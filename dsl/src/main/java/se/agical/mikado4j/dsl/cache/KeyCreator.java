package se.agical.mikado4j.dsl.cache;

public interface KeyCreator<C, O> {
	Key<O> createKey(C context);
}
