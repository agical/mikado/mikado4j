package se.agical.mikado4j.dsl.resolve;

public interface ResolutionDefiner<RC, CC> {
	FinishResolution<RC> build(ResolutionBuilder<RC, CC> builder);
}
