package se.agical.mikado4j.dsl;

public interface Task {
	void apply();
}
