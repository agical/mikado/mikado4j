package se.agical.mikado4j.dsl;

import java.util.stream.Stream;

import se.agical.mikado4j.dsl.json.Node;
import se.agical.mikado4j.dsl.locate.Locator;
import se.agical.mikado4j.dsl.resolve.Resolution;

public class MikadoNode<RC> {

	private final String goalDescription;
	private final MikadoNode<RC>[] prereqs;
	private final Resolution<RC>[] resolutions;
	private final CardinalityResolver<RC> cardinalityResolver;
	private final DoneResolver<RC> doneResolver;

	@SafeVarargs
	public MikadoNode(
			String goalDescription,
			CardinalityResolver<RC> cardinalityResolver,
			Resolution<RC>[] resolutions,
			DoneResolver<RC> doneChecker,
			MikadoNode<RC>... prereqs) {
		this.goalDescription = goalDescription;
		this.cardinalityResolver = cardinalityResolver;
		this.resolutions = resolutions;
		this.doneResolver = doneChecker;
		this.prereqs = prereqs;
	}

	public Node getDom() {
		return new Node(
				goalDescription,
				Stream.of(prereqs)
						.map(MikadoNode::getDom)
						.toArray(Node[]::new));
	}

	public Node analyze(RC context) {
		int cardinality = cardinalityResolver.cardinality(context);

		return new Node(goalDescription, cardinality, doneResolver.checkIfDone(context),
				Stream.of(prereqs)
						.map(n -> n.analyze(context))
						.toArray(Node[]::new));
	}

	@SuppressWarnings("unchecked")
	public <NC> MikadoNode<NC> viaLocator(Locator<NC, RC> locator) {
		return new MikadoNode<NC>(
				goalDescription,
				cardinalityResolver.viaLocator(locator),
				Stream.of(resolutions).map(r -> r.viaLocator(locator)).toArray(Resolution[]::new),
				doneResolver.viaLocator(locator),
				Stream.of(prereqs).map(pr -> pr.viaLocator(locator)).toArray(MikadoNode[]::new));
	}

	public Stream<Task> previewResolve(RC context) {
		return previewResolve(0, context);
	}

	private Stream<Task> previewResolve(int indent, RC context) {
		String indentStr = Stream.generate(() -> "    ").limit(indent).reduce((l, r) -> l + r).orElse("");
		String subIndentStr = Stream.generate(() -> "    ").limit(indent + 1).reduce((l, r) -> l + r).orElse("");

		boolean done = isMarkedDone(context);
		System.out.printf("%s* %s[%d][%s]:\n", indentStr, goalDescription, cardinalityResolver.cardinality(context),
				done ? 'v' : '-');

		if (!done) {
			if (hasPrereqsLeftToDo(context)) {
				for (Resolution<RC> resolution : resolutions) {
					resolution.previewResolveShort(subIndentStr, context, 'B');
				}
			} else {
				for (Resolution<RC> resolution : resolutions) {
					resolution.previewResolveLong(subIndentStr, context, ' ');
				}
			}
		}

		System.out.println();

		++indent;
		boolean canBeExecuted = !hasPrereqsLeftToDo(context) && !isThisNodeDone(context);

		Stream<Task> solutionsForThisNode = canBeExecuted
				? Stream.of(resolutions)
						.flatMap((Resolution<RC> r) -> r.maybeSolution.stream())
						.map(s -> () -> s.solve(context))
				: Stream.of();

		Stream<Task> solutionsForPrereqs = Stream.of(prereqs).flatMap(prereq -> prereq.previewResolve(context));

		return Stream.concat(solutionsForPrereqs, solutionsForThisNode);
	}

	private boolean hasPrereqsLeftToDo(RC context) {
		boolean prereqsUnFinished = Stream.of(prereqs).parallel()
				.anyMatch(prereq -> !prereq.isMarkedDone(context));
		return prereqsUnFinished;
	}

	private boolean isMarkedDone(RC context) {
		return !hasPrereqsLeftToDo(context) && isThisNodeDone(context);
	}

	private boolean isThisNodeDone(RC context) {
		return doneResolver.checkIfDone(context);
	}

}
