package se.agical.mikado4j.dsl.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import se.agical.mikado4j.dsl.locate.Locator;
import se.agical.mikado4j.dsl.locate.Many;

public class MemoizationCache {
	private static int id = 0;
	private final Lock lock = new ReentrantLock();
	private final Map<Key<?>, Many<?>> cache = new HashMap<>();

	public MemoizationCache() {
		id++;
	}

	@SuppressWarnings("unchecked")
	public <C, O> Locator<C, O> wrap(final KeyCreator<C, O> keyCreator, final Locator<C, O> locate) {
		return c -> {
			final Key<O> key = keyCreator.createKey(c);
			lock.lock();
			try {
				if (!cache.containsKey(key)) {
					System.out.println("miss in " + id + " : " + key);
				}
				return (Many<O>) cache.computeIfAbsent(key, k -> locate.locate(c));
			} finally {
				lock.unlock();
			}
		};
	}

}
