package se.agical.mikado4j.spoon;

import java.nio.file.Path;
import java.nio.file.Paths;

import spoon.Launcher;
import spoon.support.sniper.SniperJavaPrettyPrinter;

public class DemoLauncher {
	public static Launcher createLauncher() {
		final Launcher launcher = new Launcher();
		final String path = Paths.get("src/test/java").normalize().toString();
		launcher.addInputResource(path);
		launcher.getFactory().getEnvironment().setPrettyPrinterCreator(() -> {
			return new SniperJavaPrettyPrinter(launcher.getFactory().getEnvironment());
		});
		final Path outputDir = Paths.get("spooned");
		launcher.setSourceOutputDirectory(outputDir.toFile());
		launcher.getEnvironment().setNoClasspath(true);
		launcher.buildModel();
		// launcher.getModel().getAllTypes().stream().forEach(c ->
		// System.out.println(c.getQualifiedName()));
		return launcher;
	}
}
