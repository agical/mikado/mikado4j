package se.agical.mikado4j.dsl;

import java.util.Map;

import se.agical.mikado4j.dsl.json.Node;
import se.agical.mikado4j.dsl.resolve.Resolution;

public class NodeAndResolutions<C> {
	public final Node analyzed;

	public final Map<Node, Resolution<C>[]> resolutions;

	public NodeAndResolutions(Node analyzed, Map<Node, Resolution<C>[]> resolutions) {
		this.analyzed = analyzed;
		this.resolutions = resolutions;

	}

	public void printPreview() {

	}

}
