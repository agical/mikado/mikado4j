package se.agical.mikado4j.dsl;

import java.util.stream.Stream;

import se.agical.mikado4j.dsl.json.Node;

public class MikadoGraph<C> {

	private MikadoNode<C> root;

	public MikadoGraph(MikadoNode<C> root) {
		this.root = root;
	}

	public Node getDom() {
		return root.getDom();
	}

	public Node analyze(C context) {
		return root.analyze(context);
	}

	public Stream<Task> previewResolve(C context) {
		return root.previewResolve(context);
	}

}
