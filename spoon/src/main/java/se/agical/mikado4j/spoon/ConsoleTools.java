package se.agical.mikado4j.spoon;

import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtElement;

public class ConsoleTools {

	public static <E extends CtElement> String clickableLocation(E element) {
		CtClass<?> cl = element instanceof CtClass<?>
				? (CtClass<?>) element
				: element.getParent((CtClass<?> c) -> true);
		var position = element.getPosition();
		return String.format("at %s (%s:%d)", cl.getQualifiedName(), position.getFile().getName(), position.getLine());
	}
}
