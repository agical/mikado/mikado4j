package example;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import se.agical.mikado4j.dsl.MikadoGraphBuilder;
import se.agical.mikado4j.dsl.Task;
import se.agical.mikado4j.dsl.locate.Locator;
import se.agical.mikado4j.dsl.locate.Many;
import se.agical.mikado4j.dsl.resolve.Solution;
import se.agical.mikado4j.dsl.select.Select;
import se.agical.mikado4j.dsl.visualize.MikadoToolConnection;
import se.agical.mikado4j.spoon.CodeCannon;
import se.agical.mikado4j.spoon.ConsoleTools;
import se.agical.mikado4j.spoon.Locators;
import spoon.reflect.CtModel;
import spoon.reflect.code.CtFieldAccess;
import spoon.reflect.code.CtInvocation;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtPackage;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.visitor.chain.CtQuery;

public class UpdateToNewTableDefinition {

	private static Solution<InvertCellFieldData> changeCellToGoViaFieldAccess() {
		return cc -> {
			cc.cellFieldAccess.replace(cc.cellFieldAccess.getFactory().createCodeSnippetExpression(
					cc.fieldDeclaration.getDeclaringType().getSimpleName() + ".TABLE."
							+ cc.fieldDeclaration.getSimpleName()
							+ ".cell()"));

		};
	}

	public static Many<InvertCellFieldData> createCellFieldInvertData(
			final CtFieldAccess<?> field,
			final CtField<?> oldFieldDeclaration) {
		return oldFieldDeclaration.getAssignment()
				.filterChildren((final CtInvocation<?> i) -> i.getArguments()
						.stream()
						.filter(field::equals)
						.findAny()
						.isPresent())
				.first() == null
						? Stream::of
						: () -> Stream.of(InvertCellFieldData.from(field, oldFieldDeclaration));

	}

	public static List<CtFieldAccess<?>> externalFieldReferences(final CtFieldAccess<?> fa) {
		final var referencedFrom = fa.getParent(CtType.class);
		return referencedFrom.equals(fa.getVariable().getDeclaringType().getTypeDeclaration())
				? Arrays.asList()
				: Arrays.asList(fa);
	}

	public static List<CtField<?>> findCellConstants(final CtClass<?> context) {
		return context.filterChildren(
				(final CtField<?> field) -> "Cell".equals(field.getType().getSimpleName()))
				.list();
	}

	public static Many<CtField<?>> findCellConstantsN(final CtClass<?> context) {
		final Locator<CtClass<?>, CtClass<?>> cellTypes = Locators.LOCATORS.packages.<CtClass<?>>rootPackageFromElement()
				.andThen(Locators.LOCATORS.packages.<CtPackage>byQualifiedName("sveadirekt.infrastructure.type.tabular")
						.classesIn()
						.andThen(
								Locators.LOCATORS.classes.<CtClass<?>>bySimpleName("Cell")
										.union(Locators.LOCATORS.classes.bySimpleName("IdCell"))
										.union(Locators.LOCATORS.classes.bySimpleName("OptionalCell"))));
		return Locators.LOCATORS.fields.byReferencedType(cellTypes).locate(context);
	}

	public static List<CtFieldAccess<?>> findCellReferences(final CtModel root) {
		final CtQuery filterChildren = root
				.filterChildren((final CtFieldAccess<?> fa) -> {
					final CtFieldReference<?> variable = fa.getVariable();
					return variable != null &&
							variable.isStatic() &&
							variable.getType() != null &&
							variable.getType().getSimpleName().contains("Cell");
				});

		return filterChildren.list();
	}

	public static Many<CtFieldAccess<?>> findFieldReferences(final CtField<?> field, final CtFieldAccess<?> fa) {
		return field.equals(fa.getVariable().getDeclaration())
				? () -> Stream.of(fa)
				: Stream::of;
	}

	public static List<CtField<?>> findOldFieldDeclaration(final CtClass<?> context) {
		return context
				.filterChildren((final CtField<?> f) -> "Field".startsWith(f.getType().getSimpleName())
						| "IdField".startsWith(f.getType().getSimpleName())
						| "OptionalField".startsWith(f.getType().getSimpleName()))
				.filterChildren((final CtField<?> f) -> {
					final var assignment = f.getAssignment();
					if (assignment == null) {
						return false;
					}
					final CtInvocation<?> fieldInvocation = assignment
							.filterChildren((final CtInvocation<?> i) -> i.getExecutable() != null)
							.filterChildren((final CtInvocation<?> i) -> "field".equals(i.getExecutable().getSimpleName()))
							.first();
					return fieldInvocation != null;
				})
				.list();
	}

	public static List<CtClass<?>> findRelevantTables(final CtModel model) {
		return model.filterChildren((final CtClass<?> clazz) -> {
			final var sc = clazz.getSuperclass();
			return sc != null && sc
					.getQualifiedName()
					.equals("sveadirekt.infrastructure.sql.model.Table");
		})
				.list();
	}

	public static Many<CtClass<?>> findRelevantTablesN(final CtModel model) {
		return Locators.LOCATORS.packages.rootPackage().andThen(
				Locators.LOCATORS.packages.<CtPackage>byQualifiedName("sveadirekt.infrastructure.sql.model")
						.classesIn()
						.bySimpleName("Table")
						.sublClasses())
				.locate(model);
	}

	public static void main(final String[] args) {
		final var findRelevantTables = Locator.wrap(UpdateToNewTableDefinition::findRelevantTablesN)
				.memoize(context -> Locator.createKey("root::tables"));

		final var findCellConstants = Locator.wrap(UpdateToNewTableDefinition::findCellConstantsN)
				.memoize(context -> Locator.createKey(context.getPath().toString()));
		final var findCellReferences = Locator.locate(UpdateToNewTableDefinition::findCellReferences)
				.memoize(context -> Locator.createKey("root::cells"));
		final var findOldFieldReference = Locator.locate(UpdateToNewTableDefinition::findOldFieldDeclaration)
				.memoize(context -> Locator.createKey(context.getPath().toString()));
		final var externalRefrerences = Locator.locate(UpdateToNewTableDefinition::externalFieldReferences);

		final var graphSimple = MikadoGraphBuilder
				.<CtModel>goal("Update to new table definition", b -> b
						.select(findRelevantTables)
						.prereq("remove unused Entities"));
		/*
		 * .prereq("remove cell constants", b2 -> b2 .select(findCellConstants)
		 * .resolve(resolution -> resolution .name("delete cell constant")
		 * .shortDescription( c -> Arrays.asList(String.format("delete: %s\n%s",
		 * c.prettyprint(), ConsoleTools.clickableLocation(c)))))
		 *
		 * .doneWhenNoneLeft()
		 *
		 * .prereq("replace all cell constant accesses with TABLE.field.cell()", b3 ->
		 * b3 .combineAbsolute(findCellReferences,
		 * UpdateToNewTableDefinition::findFieldReferences) .select(externalRefrerences)
		 * .selectAbsolute(b2.locator().andThen(findOldFieldReference)) .combine(
		 * UpdateToNewTableDefinition::createCellFieldInvertData) .resolve(resolution ->
		 * resolution .name("change cell to via field access") .shortDescription(d ->
		 * Arrays.asList(d.describe())) .solveWith(changeCellToGoViaFieldAccess()))
		 * .doneWhenNoneLeft())
		 *
		 * .prereq("update field syntax to dbField(name).javaType()-style", b3 -> b3
		 * .selectAbsolute(b2.locator().andThen(findOldFieldReference))
		 * .doneWhenNoneLeft() .prereq("db: lookup actual field type",
		 * Select::notDone))));
		 */
		final var graph = MikadoGraphBuilder
				.<CtModel>goal("Update to new table definition", b -> b
						.select(findRelevantTables)
						.prereq("remove unused Entities")
						.prereq("remove cell constants", b2 -> b2
								.select(findCellConstants)
								.resolve(resolution -> resolution
										.name("delete cell constant")
										.shortDescription(
												c -> Arrays.asList(String.format("delete: %s\n%s",
														c.prettyprint(),
														ConsoleTools.clickableLocation(c)))))

								.doneWhenNoneLeft()

								.prereq("replace all cell constant accesses with TABLE.field.cell()", b3 -> b3
										.combineAbsolute(findCellReferences,
												UpdateToNewTableDefinition::findFieldReferences)
										.select(externalRefrerences)
										.selectAbsolute(b2.locator().andThen(findOldFieldReference))
										.combine(
												UpdateToNewTableDefinition::createCellFieldInvertData)
										.resolve(resolution -> resolution
												.name("change cell to via field access")
												.shortDescription(d -> Arrays.asList(d.describe()))
												.solveWith(changeCellToGoViaFieldAccess()))
										.doneWhenNoneLeft())

								.prereq("update field syntax to dbField(name).javaType()-style", b3 -> b3
										.selectAbsolute(b2.locator().andThen(findOldFieldReference))
										.doneWhenNoneLeft()
										.prereq("db: lookup actual field type", Select::notDone))));

		try {
			new CodeCannon(Paths.get("../../sveasource"))
					.excludePathsNotContaining("dbmodel", "infrastructure")
					// .excludePathsNotContaining("account")
					// .excludePathContaining("account")
					// .excludePathContaining("infrastructure")
					// .excludePathContaining("application")
					// .excludePathContaining("loan")
					// .excludePathContaining("batch")
					// .excludePathContaining("doc")
					// .excludePathContaining("foliage")
					// .excludePathContaining("identification")
					// .excludePathContaining("codecannon")
//                    .excludePathsNotContaining("sveadirekt") // funkar ej
					.excludePathContaining(".test.")
					.startAnalysis()
					.transform((final CtModel model) -> {
						final MikadoToolConnection mikadoToolConnection = new MikadoToolConnection("br7012jwajbehli2g8esl");
						final var start = System.currentTimeMillis();
						System.out.println("Begin transformation");
						final var tasks = graph.previewResolve(model);
						System.out.println("");
						System.out.printf("End preview after %dms\n", System.currentTimeMillis() - start);

						tasks.collect(Collectors.toList()).forEach(Task::apply);
						mikadoToolConnection.publish(graph.analyze(model));
						System.out.printf("End transformation after %dms\n", System.currentTimeMillis() - start);

						return model;
					})
					.makeChangesInTemporaryLocation();
			// .writeChangesToSourceLocation();

			// .collect(Collectors.toList());
		} catch (final IOException e) {
			e.printStackTrace();
		}

	}

}
