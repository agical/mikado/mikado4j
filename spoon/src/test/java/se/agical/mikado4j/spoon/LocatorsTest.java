package se.agical.mikado4j.spoon;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static se.agical.mikado4j.spoon.Locators.LOCATORS;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import se.agical.mikado4j.spoon.example.Caller;
import spoon.Launcher;
import spoon.reflect.code.CtFieldAccess;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtPackage;

public class LocatorsTest {
	@Nested
	class Classes {
		@Test
		void findsClassBySimpleName() throws Exception {
			final var foundClasses = LOCATORS.modelFromLauncher()
					.andThen(LOCATORS.packages.rootPackage())
					.andThen(LOCATORS.classes.bySimpleName("AClass"))
					.locate(launcher);

			assertEquals(2, foundClasses.stream().count());
		}

		@Test
		void findsClassBySimpleNameInPackage() throws Exception {
			final var foundClasses = LOCATORS.modelFromLauncher()
					.andThen(LOCATORS.packages.rootPackage())
					.andThen(LOCATORS.classes
							.inPackage(LOCATORS.packages.byQualifiedName("se.agical.mikado4j.spoon.example")))
					.andThen(LOCATORS.classes.bySimpleName("AClass"))
					.locate(launcher);

			assertEquals(1, foundClasses.stream().count());
		}

		@Test
		void findsClassBySimpleNameInSubPackageOfExample() throws Exception {
			final var foundClasses = LOCATORS.modelFromLauncher()
					.andThen(LOCATORS.packages.rootPackage())
					.andThen(LOCATORS.classes
							.inPackage(
									LOCATORS.packages.under(
											LOCATORS.packages.byQualifiedName("se.agical.mikado4j.spoon.example"))))
					.andThen(LOCATORS.classes.bySimpleName("AClass"))
					.locate(launcher);

			assertEquals(
					Stream.of("se.agical.mikado4j.spoon.example.subPackage.AClass").collect(Collectors.toList()),
					foundClasses.stream().map(CtClass::getQualifiedName).collect(Collectors.toList()));
			assertEquals(1, foundClasses.stream().count());
		}

		@Test
		void findsDirectSubClassesOfAClass() {
			final var foundClasses = LOCATORS.modelFromLauncher()
					.andThen(LOCATORS.packages.rootPackage())
					.andThen(LOCATORS.classes.inPackage(LOCATORS.packages.bySimpleName("example")))
					.andThen(LOCATORS.classes.bySimpleName("AClass"))
					.andThen(LOCATORS.classes.subClasses())
					.locate(launcher);

			assertEquals(
					Stream.of("ASubClass").collect(Collectors.toSet()),
					foundClasses.stream().map(CtClass::getSimpleName).collect(Collectors.toSet()));
			assertEquals(1, foundClasses.stream().count());

		}
	}

	@Nested
	class FieldAccesses {
		@Test
		void inClass() throws Exception {
			final var foundFieldAccesses = LOCATORS.modelFromLauncher()
					.andThen(LOCATORS.packages.rootPackage())
					.andThen(LOCATORS.classes.inPackage(LOCATORS.packages.bySimpleName("example")))
					.andThen(LOCATORS.classes.bySimpleName(Caller.class.getSimpleName()))
					.andThen(LOCATORS.fieldReferences.all())
					.locate(launcher);

			assertEquals(Stream.of("callee", "fielder.aClassField", "callee.referencedField", "fielder").collect(Collectors.toSet()),
					foundFieldAccesses.stream().map(CtFieldAccess::toStringDebug).collect(Collectors.toSet()));
		}
	}

	@Nested
	class Fields {
		@Test
		void findsFieldsByName() throws Exception {
			final var foundClasses = LOCATORS.modelFromLauncher()
					.andThen(LOCATORS.packages.rootPackage())
					.andThen(LOCATORS.fields.bySimpleName("privateStaticString"))
					.locate(launcher);

			assertEquals(
					Stream.of("privateStaticString").collect(Collectors.toSet()),
					foundClasses.stream().map(CtField::getSimpleName).collect(Collectors.toSet()));
		}

		@Test
		void findsFieldsByType() throws Exception {
			final var foundClasses = LOCATORS.modelFromLauncher()
					.andThen(LOCATORS.packages.rootPackage())
					.andThen(LOCATORS.fields.byReferencedType(LOCATORS.classes.bySimpleName("ASubClass")))
					.locate(launcher);

			assertEquals(
					Stream.of("aSubClassField").collect(Collectors.toSet()),
					foundClasses.stream().map(CtField::getSimpleName).collect(Collectors.toSet()));
		}
	}

	@Nested
	class Packages {
		@Test
		void byQualifiedName() {
			final var foundPackages = LOCATORS.modelFromLauncher()
					.andThen(LOCATORS.packages.rootPackage())
					.andThen(LOCATORS.packages.byQualifiedName("se.agical.mikado4j.spoon.example"))
					.locate(launcher);
			assertEquals(1, foundPackages.stream().count());
		}

		@Test
		void bySimpleName() {
			final var foundPackages = LOCATORS.modelFromLauncher()
					.andThen(LOCATORS.packages.rootPackage())
					.andThen(LOCATORS.packages.bySimpleName("example"))
					.locate(launcher);
			assertEquals(1, foundPackages.stream().count());
		}

		@Test
		void findRootFromElement() throws Exception {
			final var rootPackage = LOCATORS.modelFromLauncher()
					.andThen(LOCATORS.packages.rootPackage())
					.locate(launcher);

			final var rootPackageViaElement = LOCATORS.modelFromLauncher()
					.andThen(LOCATORS.packages.rootPackage())
					.andThen(LOCATORS.packages.bySimpleName("example"))
					.andThen(LOCATORS.packages.rootPackageFromElement())
					.locate(launcher);

			assertArrayEquals(
					rootPackage.stream().map(CtPackage::getQualifiedName).toArray(String[]::new),
					rootPackageViaElement.stream().map(CtPackage::getQualifiedName).toArray(String[]::new));
		}

		@Test
		void rootPackage() throws Exception {
			final var foundPackages = LOCATORS.modelFromLauncher()
					.andThen(LOCATORS.packages.rootPackage())
					.locate(launcher);
			assertEquals(1, foundPackages.stream().count());
		}

		@Test
		void underQualifiedName() {
			final var foundPackages = LOCATORS.modelFromLauncher()
					.andThen(LOCATORS.packages.rootPackage())
					.andThen(LOCATORS.packages
							.under(LOCATORS.packages.byQualifiedName("se.agical.mikado4j.spoon.example")))
					.locate(launcher);
			assertEquals(
					Stream.of("se.agical.mikado4j.spoon.example.subPackage").collect(Collectors.toList()),
					foundPackages.stream().map(CtPackage::getQualifiedName).collect(Collectors.toList()));
		}

	}

	Launcher launcher = DemoLauncher.createLauncher();
}
