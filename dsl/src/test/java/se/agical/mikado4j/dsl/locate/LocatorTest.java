package se.agical.mikado4j.dsl.locate;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

public class LocatorTest {
	static final Locator<List<Integer>, Integer> locateNumbersBelow8 = allNumbers -> () -> allNumbers.stream().filter(n -> n < 8);
	static final Locator<List<Integer>, Integer> locateNumbersAbove4 = allNumbers -> () -> allNumbers.stream().filter(n -> n > 4);

	@Test
	void differenceCombinesTwoLocatorsOfTheSameType() throws Exception {

		final List<Integer> numbers = locateNumbersAbove4
				.difference(locateNumbersBelow8)
				.locate(oneToTen())
				.stream()
				.sorted()
				.collect(Collectors.toList());

		assertEquals(
				Stream.of(1, 2, 3, 4, 8, 9, 10).collect(Collectors.toList()),
				numbers);
	}

	@Test
	void intersectReturnsAllNonUniqeElementsFromTwoLocatorsTwoLocators() throws Exception {
		final List<Integer> numbers = locateNumbersAbove4
				.intersect(locateNumbersBelow8)
				.locate(oneToTen())
				.stream()
				.sorted()
				.collect(Collectors.toList());

		assertEquals(
				Stream.of(5, 6, 7).collect(Collectors.toList()),

				numbers);
	}

	private List<Integer> oneToTen() {
		return Stream.iterate(1, i -> i + 1).limit(10).collect(Collectors.toList());
	}

	@Test
	void unionReturnsAllUniqeElementsFromTwoLocatorsTwoLocators() throws Exception {
		final List<Integer> numbers = locateNumbersAbove4
				.union(locateNumbersBelow8)
				.locate(oneToTen())
				.stream()
				.sorted()
				.collect(Collectors.toList());

		assertEquals(
				oneToTen().stream().sorted().collect(Collectors.toList()),
				numbers);
	}

}
