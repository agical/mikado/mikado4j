package se.agical.mikado4j.dsl.resolve;

import se.agical.mikado4j.dsl.locate.Locator;

@FunctionalInterface
public interface Solution<CC> {

	void solve(CC context);

	default <RC> Solution<RC> viaLocator(Locator<RC, CC> locator) {
		return rc -> locator
				.locate(rc)
				.stream()
				.forEach(cc -> {
					solve(cc);
				});
	};

}
