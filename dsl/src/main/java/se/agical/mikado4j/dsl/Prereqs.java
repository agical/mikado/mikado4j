package se.agical.mikado4j.dsl;

import java.util.ArrayList;

import se.agical.mikado4j.dsl.locate.Locator;

public interface Prereqs<RC, CC> extends FinishNode<RC> {
	Locator<RC, CC> locator();

	Prereqs<RC, CC> prereq(MikadoNode<RC> prereq);

	default Prereqs<RC, CC> prereq(String description) {
		return prereq(description, b -> b);
	}

	default Prereqs<RC, CC> prereq(String description, PrereqDefiner<RC, CC> prereqDefiner) {
		MikadoNodeBuilder<RC, CC> prereqBuilder = new MikadoNodeBuilder<>(description, locator(), new ArrayList<>(),
				new ArrayList<>());
		FinishNode<RC> prereqs = prereqDefiner.buildPrereq(prereqBuilder);
		return prereq(prereqs.finish());
	}
}
