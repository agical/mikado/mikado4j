package se.agical.mikado4j.spoon;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import se.agical.mikado4j.dsl.cache.Key;
import se.agical.mikado4j.dsl.cache.KeyCreator;
import se.agical.mikado4j.dsl.cache.MemoizationCache;
import se.agical.mikado4j.dsl.locate.Locator;
import se.agical.mikado4j.dsl.locate.Many;
import se.agical.mikado4j.spoon.Locators.Classes.ClassLocator;
import spoon.Launcher;
import spoon.reflect.CtModel;
import spoon.reflect.code.CtFieldAccess;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtElement;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtPackage;
import spoon.reflect.visitor.CtAbstractVisitor;
import spoon.reflect.visitor.CtVisitable;
import spoon.reflect.visitor.chain.CtQuery;

public class Locators {

	public class Classes {
		public class ClassLocator<C extends CtVisitable> implements Locator<C, CtClass<?>> {
			Locator<C, CtClass<?>> locator;

			public ClassLocator(final Locator<C, CtClass<?>> locator) {
				this.locator = locator;
			}

			public ClassLocator<C> bySimpleName(final String name) {
				return new ClassLocator<>(locator.andThen(classes.bySimpleName(name)));
			}

			@Override
			public Many<CtClass<?>> locate(final C context) {
				return locator.locate(context);
			}

			public ClassLocator<C> sublClasses() {
				return new ClassLocator<>(locator.andThen(classes.subClasses()));
			}
		}

		private Classes() {
		}

		public <C extends CtElement> ClassLocator<C> bySimpleName(final String name) {
			return classLocator(
					(final C c) -> queryToMany(c.filterChildren((final CtClass<?> clazz) -> name.equals(clazz.getSimpleName()))));
		}

		private <C extends CtElement> KeyCreator<C, CtClass<?>> classKey(final String cacheKey) {
			return c -> new Key<>(new ClassKey(c, cacheKey));
		}

		private <C extends CtVisitable> ClassLocator<C> classLocator(final Locator<C, CtClass<?>> locator) {
			return new ClassLocator<>(locator);
		}

		public <C extends CtVisitable> ClassLocator<C> inPackage(final Locator<C, CtPackage> packageLocator) {
			return classLocator(packageLocator.combine(
					memoize(p -> queryToMany(
							p.filterChildren((final CtClass<?> c) -> {
								final var cpkg = c.getPackage();
								return cpkg != null && cpkg.equals(p);
							})), classKey("in package")),
					(final CtPackage p, final CtClass<?> c) -> () -> Stream.of(c)));
		}

		public Locator<CtClass<?>, CtClass<?>> subClasses() {
			return Locator.wrap((final CtClass<?> c) -> () -> Stream.of(c))
					.combineFromRoot(
							LOCATORS.classes.<CtClass<?>>underPackage(LOCATORS.packages.rootPackageFromElement()),
							(superClass, potentialSubClass) -> () -> {
								final var isDirectSubClass = potentialSubClass
										.getSuperclass() != null && superClass
												.equals(potentialSubClass.getSuperclass().getDeclaration());
								return isDirectSubClass
										? Stream.of(potentialSubClass)
										: Stream.of();
							});
		}

		public <C extends CtVisitable> ClassLocator<C> underPackage(final Locator<C, CtPackage> packageLocator) {
			return classLocator(packageLocator.andThen(p -> queryToMany(p.filterChildren((final CtClass<?> c) -> true))));
		}
	}

	private static class ClassKey {
		CtElement context;
		String key;

		public ClassKey(final CtElement context, final String key) {
			super();
			this.context = context;
			this.key = key;
		}

		@Override
		public boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final ClassKey other = (ClassKey) obj;
			if (!Objects.equals(context, other.context)) {
				return false;
			}
			if (!Objects.equals(key, other.key)) {
				return false;
			}
			return true;
		}

		@Override
		public int hashCode() {
			return Objects.hash(context, key);
		}

		@Override
		public String toString() {
			return String.format("ClassKey {%s, %s}", key, context.getShortRepresentation());
		}

	}

	private static class FieldAccessKey {
		CtElement context;
		String key;

		public FieldAccessKey(final CtElement context, final String key) {
			super();
			this.context = context;
			this.key = key;
		}

		@Override
		public boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final FieldAccessKey other = (FieldAccessKey) obj;
			if (!Objects.equals(context, other.context)) {
				return false;
			}
			if (!Objects.equals(key, other.key)) {
				return false;
			}
			return true;
		}

		@Override
		public int hashCode() {
			return Objects.hash(context, key);
		}

		@Override
		public String toString() {
			return String.format("FieldAccessKey {%s, %s}", key, context.getShortRepresentation());
		}

	}

	public class FieldReferences {
		private FieldReferences() {

		}

		public <C extends CtElement> Locator<C, CtFieldAccess<?>> all() {
			return memoize(
					c -> queryToMany(c.filterChildren((final CtFieldAccess<?> fa) -> true)),
					fieldAccessKey("all"));
		}

		private <C extends CtElement> KeyCreator<C, CtFieldAccess<?>> fieldAccessKey(final String key) {
			return (final C c) -> new Key<>(new FieldAccessKey(c, key));
		}

	}

	public class Fields {
		private Fields() {

		}

		public <C extends CtElement> Locator<C, CtField<?>> byReferencedType(final Locator<C, CtClass<?>> type) {
			return type.<CtField<?>, CtField<?>>combineFromRoot(
					(final C c) -> queryToMany(c.filterChildren((final CtField<?> f) -> true)),
					(final CtClass<?> t, final CtField<?> f) -> () -> {
						final var td = f.getType().getTypeDeclaration();
						return td != null && td.equals(t) ? Stream.of(f) : Stream.of();
					});
		}

		public <C extends CtElement> Locator<C, CtField<?>> bySimpleName(final String simpleName) {
			return c -> queryToMany(c.filterChildren((final CtField<?> clazz) -> simpleName.equals(clazz.getSimpleName())));
		}
	}

	private static class PackageKey {
		CtPackage context;
		String key;

		public PackageKey(final CtPackage context, final String key) {
			super();
			this.context = context;
			this.key = key;
		}

		@Override
		public boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final PackageKey other = (PackageKey) obj;
			if (!Objects.equals(context, other.context)) {
				return false;
			}
			if (!Objects.equals(key, other.key)) {
				return false;
			}
			return true;
		}

		@Override
		public int hashCode() {
			return Objects.hash(context, key);
		}

		@Override
		public String toString() {
			return String.format("PackageKey {%s, %s}", key, context.getShortRepresentation());
		}

	}

	public class Packages {
		public class PackageLocator<C extends CtVisitable> implements Locator<C, CtPackage> {
			public class ClassExtender {

			}

			private final Locator<C, CtPackage> locator;

			public PackageLocator(final Locator<C, CtPackage> locator) {
				this.locator = locator;
			}

			public <O> Classes.ClassLocator<C> classesIn() {
				return classes.inPackage(locator);
			}

			public <O> ClassLocator<C> classesUnder() {
				return classes.underPackage(locator);
			}

			@Override
			public Many<CtPackage> locate(final C context) {
				return locator.locate(context);
			}
		}

		private class RootPackageVisitor extends CtAbstractVisitor {
			public Optional<CtPackage> rootPackage = Optional.empty();

			@Override
			public void visitCtPackage(final CtPackage p) {
				if ("".equals(p.getQualifiedName())) {
					rootPackage = Optional.of(p);
				} else {
					p.getParent().accept(this);
				}
			}
		}

		private Packages() {
		}

		public <C extends CtVisitable> PackageLocator<C> byQualifiedName(final String name) {
			return packageLocator(this.<C>rootPackageFromElement()
					.andThen(memoize(c -> queryToMany(c.filterChildren((final CtPackage p) -> name.equals(p.getQualifiedName()))),
							packageKey("byQualifiedName:" + name))));
		}

		public <C extends CtPackage> PackageLocator<C> bySimpleName(final String name) {
			return packageLocator(memoize(c -> queryToMany(c.filterChildren((final CtPackage p) -> name.equals(p.getSimpleName()))), packageKey("bySimpleName:" + name)));
		}

		private <C extends CtPackage> KeyCreator<C, CtPackage> packageKey(final String cacheKey) {
			return c -> new Key<>(new PackageKey(c, cacheKey));
		}

		private <C extends CtVisitable> PackageLocator<C> packageLocator(final Locator<C, CtPackage> locator) {
			return new PackageLocator<>(locator);
		}

		public Locator<CtModel, CtPackage> rootPackage() {
			return c -> () -> Stream.of(c.getRootPackage());
		}

		public <C extends CtVisitable> PackageLocator<C> rootPackageFromElement() {
			return packageLocator(c -> () -> {
				final RootPackageVisitor findRootPackage = new RootPackageVisitor();
				c.accept(findRootPackage);
				return findRootPackage.rootPackage.or(
						() -> (c instanceof CtElement
								? Optional.ofNullable(((CtElement) c).getParent((final CtPackage p) -> "".equals(p.getQualifiedName())))
								: Optional.empty()))
						.stream();
			});
		}

		public <C extends CtElement> PackageLocator<C> under(final PackageLocator<C> packages) {
			return packageLocator(packages.<CtPackage>andThen((final CtPackage p) -> queryToMany(p.filterChildren((final CtPackage sp) -> !sp.equals(p)))));
		}
	}

	public static final Locators LOCATORS = new Locators();

	@SuppressWarnings("unchecked")
	private static <T> Many<T> queryToMany(final CtQuery query) {
		return () -> query.list().stream().map(e -> (T) e);
	}

	public final Classes classes = new Classes();

	public final Packages packages = new Packages();

	public final Fields fields = new Fields();

	public final FieldReferences fieldReferences = new FieldReferences();

	MemoizationCache cache = new MemoizationCache();

	private Locators() {
	}

	private <C extends CtVisitable, O> Locator<C, O> memoize(final Locator<C, O> locator, final KeyCreator<C, O> generator) {
		return cache.wrap(generator, locator);
	}

	public Locator<Launcher, CtModel> modelFromLauncher() {
		return Locator.<Launcher, CtModel>wrap(
				l -> {
					final var model = l.getModel();
					final var builtModel = model == null ? l.buildModel() : model;
					return () -> Stream.of(builtModel);
				})
				.memoize(l -> new Key<>("launcher"));
	}
}
