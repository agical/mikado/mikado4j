package se.agical.mikado4j.dsl.select;

import se.agical.mikado4j.dsl.locate.Combine2;
import se.agical.mikado4j.dsl.locate.Locator;
import se.agical.mikado4j.dsl.resolve.Resolutions;

public interface Select<RC, CC> extends Resolutions<RC, CC> {

	<NC> Select<RC, NC> updateSelection(Locator<RC, NC> newSelection);

	default Selected1<RC, CC> selectCurrent() {
		return new Selected1<>(this);
	}

	default <NC> Selected1<RC, NC> select(Locator<CC, NC> resolution) {
		return selectAbsolute(locator().andThen(resolution));
	}

	default <NC> Selected1<RC, NC> selectAbsolute(Locator<RC, NC> resolution) {
		return new Selected1<>(updateSelection(resolution));
	}

	default <NC, CMB> Select<RC, CMB> combineAbsolute(Locator<RC, NC> nextLocator, Combine2<CC, NC, CMB> combiner) {
		return updateSelection(locator().combineFromRoot(nextLocator, combiner));
	}

}
