package se.agical.mikado4j.dsl.visualize;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

import com.google.gson.Gson;

import se.agical.mikado4j.dsl.MikadoGraph;
import se.agical.mikado4j.dsl.json.Node;

public class MikadoToolConnection {
	String MIKADO_TOOL_PATTERN = "https://tool.mikadomethod.info/api/post/%s";
	private String graphPrefix;

	public MikadoToolConnection(String graphPrefix) {
		this.graphPrefix = graphPrefix;
	}

	public void publish(Node graph) {
		try {
			URL url = new URL(String.format(MIKADO_TOOL_PATTERN, graphPrefix));
			System.out.println(url);
			URLConnection con = url.openConnection();
			HttpURLConnection http = (HttpURLConnection) con;
			http.setRequestMethod("POST"); // PUT is another valid option
			http.setDoOutput(true);

			String json = new Gson().toJson(graph);
			System.out.printf("sending: %s\n", json);
			byte[] out = json.getBytes(StandardCharsets.UTF_8);
			int length = out.length;

			http.setFixedLengthStreamingMode(length);
			http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			http.connect();
			try (OutputStream os = http.getOutputStream()) {
				os.write(out);
			}
			http.disconnect();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static class Request {
		String id;
		Node document;

		public Request(String id, Node document) {
			super();
			this.id = id;
			this.document = document;
		}

	}

	public <RC> void publishAnalyzed(MikadoGraph<RC> graph, RC model) {
		publish(graph.analyze(model));
	}

}
