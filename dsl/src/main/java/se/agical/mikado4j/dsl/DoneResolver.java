package se.agical.mikado4j.dsl;

import java.util.Arrays;

import se.agical.mikado4j.dsl.locate.Locator;
import se.agical.mikado4j.dsl.resolve.DoneChecker;

public class DoneResolver<RC> {
	private final DoneChecker<RC> doneChecker;

	public boolean checkIfDone(RC context) {
		return doneChecker.checkIfDone(Arrays.asList(context));
	}

	public DoneResolver(DoneChecker<RC> doneChecker) {
		this.doneChecker = doneChecker;
	}

	public <NC> DoneResolver<NC> viaLocator(Locator<NC, RC> locator) {
		return new DoneResolver<>(doneChecker.viaLocator(locator));
	}

}
