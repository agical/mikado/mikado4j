package se.agical.mikado4j.spoon;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import spoon.Launcher;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtClass;

public class ConsoleToolsTest {

	static Launcher demoLauncher = DemoLauncher.createLauncher();
	static CtModel demoModel = demoLauncher.getModel();

	@Test
	void createClickableLocaionOfClass() throws Exception {
		CtClass<?> calleeClass = demoModel.filterChildren((CtClass<?> c) -> "Callee".equals(c.getSimpleName())).first();
		assertEquals("at se.agical.mikado4j.spoon.example.Callee (Callee.java:3)",
				ConsoleTools.clickableLocation(calleeClass));
	}

}
