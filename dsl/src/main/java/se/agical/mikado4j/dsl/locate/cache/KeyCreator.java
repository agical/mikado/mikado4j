package se.agical.mikado4j.dsl.locate.cache;

public interface KeyCreator<C, O> {
	Key<O> createKey(C context);
}
