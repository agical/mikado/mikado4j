package se.agical.mikado4j.dsl.locate;

import java.util.ArrayList;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Stream;

@FunctionalInterface
public interface Many<T> {
	Stream<T> stream();

	static <T> Many<T> from(Many<T> many) {
		return many;
	}

	static <T> Many<T> empty() {
		return () -> Stream.empty();
	}

	static <T> Collector<T, ?, Many<T>> collector() {
		Supplier<ArrayList<T>> supplier = () -> new ArrayList<>();
		BiConsumer<ArrayList<T>, T> accumulator = (a, t) -> {
			a.add(t);
		};
		BinaryOperator<ArrayList<T>> combiner = (l, r) -> {
			l.addAll(r);
			return l;
		};

		return Collector.<T, ArrayList<T>, Many<T>>of(
				supplier,
				accumulator,
				combiner,
				(ArrayList<T> a) -> () -> a.stream());
	};
}
