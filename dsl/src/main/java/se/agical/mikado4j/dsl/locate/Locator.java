package se.agical.mikado4j.dsl.locate;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import se.agical.mikado4j.dsl.cache.Key;
import se.agical.mikado4j.dsl.cache.KeyCreator;
import se.agical.mikado4j.dsl.cache.MemoizationCache;

/**
 * A locate finds elements on the context
 *
 *
 *
 */
public interface Locator<C, O> {
	static <O> Key<O> createKey(final Object uniqueKey) {
		return new Key<>(uniqueKey);
	}

	static <C, O> Locator<C, O> locate(final Function<C, List<O>> locator) {
		return Locator.wrap((final C c) -> {
			final var located = locator.apply(c);
			return () -> located.stream();
		});
	}

	static <C, O> Locator<C, O> wrap(final Locator<C, O> locator) {
		return locator;
	}

	default <NO> Locator<C, NO> andThen(final Locator<O, NO> resolution) {

		return c -> {
			final var located = this.locate(c)
					.stream()
					.parallel()
					.flatMap(nc -> {
						try {
							return resolution.locate(nc).stream();
						} catch (final RuntimeException re) {
							System.err.printf("Failed to resolve %s: %s\n", nc, re.getMessage());
							throw re;
						}
					}).collect(Collectors.toList());
			return () -> located.stream();
		};
	}

	default <NC, CMB> Locator<C, CMB> combine(final Locator<O, NC> rightLocator, final Combine2<O, NC, CMB> combiner) {
		return combineFromRoot(andThen(rightLocator), combiner);
	}

	default <NC, CMB> Locator<C, CMB> combineFromRoot(final Locator<C, NC> rightLocator, final Combine2<O, NC, CMB> combiner) {
		return c -> {
			final Many<O> lefts = locate(c);
			final Many<NC> rights = rightLocator.locate(c);
			return () -> lefts.stream()
					.flatMap(l -> rights.stream()
							.flatMap(r -> combiner.combine(l, r).stream()));
		};
	}

	default Locator<C, O> difference(final Locator<C, O> other) {
		return (final C c) -> {
			final Set<O> left = new LinkedHashSet<>();
			final Set<O> right = new LinkedHashSet<>();

			this.locate(c).stream().forEach(left::add);
			other.locate(c).stream().forEach(right::add);

			final Set<O> leftCopy = new LinkedHashSet<>(left);
			left.removeAll(right);
			right.removeAll(leftCopy);

			left.addAll(right);
			return () -> left.stream();
		};
	}

	default Locator<C, O> intersect(final Locator<C, O> other) {
		return (final C c) -> {
			final Set<O> left = new LinkedHashSet<>();
			final Set<O> right = new LinkedHashSet<>();

			this.locate(c).stream().forEach(left::add);
			other.locate(c).stream().filter(n -> left.contains(n)).forEach(right::add);

			return () -> right.stream();
		};
	}

	Many<O> locate(C context);

	default Locator<C, O> memoize(final KeyCreator<C, O> keyCreator) {
		final var cache = new MemoizationCache();
		return cache.wrap(keyCreator, this);
	}

	default Locator<C, O> union(final Locator<C, O> other) {
		return (final C c) -> {
			final Set<O> left = new LinkedHashSet<>();
			final Set<O> right = new LinkedHashSet<>();

			this.locate(c).stream().forEach(left::add);
			other.locate(c).stream().forEach(right::add);

			left.addAll(right);
			return () -> left.stream();
		};
	}
}
