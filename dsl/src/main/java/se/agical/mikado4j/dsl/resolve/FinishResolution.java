package se.agical.mikado4j.dsl.resolve;

public interface FinishResolution<RC> {

	Resolution<RC> finish();

}
