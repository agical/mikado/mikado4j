package se.agical.mikado4j.dsl;

import se.agical.mikado4j.dsl.select.Select;

@FunctionalInterface
public interface PrereqDefiner<RC, C> {
	FinishNode<RC> buildPrereq(Select<RC, C> builder);

}
