package se.agical.mikado4j.dsl;

public interface FinishNode<T> {
	MikadoNode<T> finish();
}
