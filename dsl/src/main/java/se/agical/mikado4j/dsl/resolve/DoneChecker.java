package se.agical.mikado4j.dsl.resolve;

import java.util.List;
import java.util.stream.Collectors;

import se.agical.mikado4j.dsl.locate.Locator;

@FunctionalInterface
public interface DoneChecker<CC> {
	boolean checkIfDone(List<CC> contexts);

	default <NC> DoneChecker<NC> viaLocator(Locator<NC, CC> locator) {
		return ncs -> checkIfDone(
				ncs
						.stream()
						.flatMap(nc -> locator.locate(nc).stream())
						.collect(Collectors.toList()));
	}
}
