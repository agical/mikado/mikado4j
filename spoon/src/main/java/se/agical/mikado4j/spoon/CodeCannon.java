package se.agical.mikado4j.spoon;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import spoon.Launcher;
import spoon.OutputType;
import spoon.reflect.CtModel;
import spoon.support.JavaOutputProcessor;
import spoon.support.sniper.SniperJavaPrettyPrinter;

public class CodeCannon {

	public static class Analyzed {
		final CtModel model;
		final Launcher launcher;
		final Path outputDir;
		private final Path workspaceRoot;

		private Analyzed(final Launcher launcher, final Path outputDir, final Path workspaceRoot) {
			this.launcher = launcher;
			this.outputDir = outputDir;
			this.workspaceRoot = workspaceRoot;
			this.model = launcher.buildModel();
		}

		public Transformed transform(final Function<CtModel, CtModel> transformer) {
			final CtModel transformedModel = transformer.apply(model);
			return new Transformed(transformedModel, this);
		}

	}

	public static class Transformed {

		private final CtModel transformedModel;
		private final Analyzed analyzed;

		public Transformed(final CtModel transformedModel, final Analyzed analyzed) {
			this.transformedModel = transformedModel;
			this.analyzed = analyzed;
		}

		public Transformed makeChangesInTemporaryLocation() {
			transformedModel.processWith(
					new JavaOutputProcessor(new SniperJavaPrettyPrinter(analyzed.launcher.getEnvironment())));
			return this;
		}

		public void writeChangesToSourceLocation() {
			try {
				final Path targetRoot = analyzed.workspaceRoot;
				System.out.println(String.format("Moving from %s to %s",
						analyzed.outputDir.normalize().toAbsolutePath(),
						analyzed.workspaceRoot.normalize().toAbsolutePath()));

				Files.walk(analyzed.outputDir)
						.filter(path -> path.toFile().toString().endsWith(".java"))
						.forEach(path -> {
							try {
								final var relativePath = path.subpath(1, path.getNameCount());
								System.out.println("path:" + path);
								System.out.println("relativePath: " + relativePath);
								final var projectSegment = relativePath.subpath(1, 3);
								final Path targetFile = targetRoot
										.resolve(projectSegment)
										.resolve("src/")
										.resolve(relativePath);
								System.out.println("Target: " + targetFile.normalize().toAbsolutePath());
								Files.copy(path, targetFile, StandardCopyOption.REPLACE_EXISTING);
							} catch (final IOException e) {
								throw new RuntimeException(e);
							}
						});
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}

	}

	private final Stream<String> pathsToBlast;

	private final Path workspaceRoot;

	public CodeCannon(final Path workspaceRoot) throws IOException {
		this(workspaceRoot, Files.walk(workspaceRoot.toAbsolutePath().normalize())
				.filter(path -> path.endsWith(Paths.get("src")))
				.map(Path::toFile)
				.map(Object::toString)
				.filter(path -> !path.contains("build_output")));
	}

	CodeCannon(final Path workspaceRoot, final Stream<String> pathsToBlast) {
		this.workspaceRoot = workspaceRoot;
		this.pathsToBlast = pathsToBlast;
	}

	private boolean containsAny(final String path, final String[] strings) {
		for (final String string : strings) {
			if (path.contains(string)) {
				return true;
			}
		}
		return false;
	}

	public CodeCannon excludePathContaining(final String string) {
		return new CodeCannon(workspaceRoot, pathsToBlast.filter(path -> !path.contains(string)));

	}

	public CodeCannon excludePathsNotContaining(final String... strings) {
		return new CodeCannon(workspaceRoot, pathsToBlast.filter(path -> containsAny(path, strings)));

	}

	public Analyzed startAnalysis() {
		final List<String> sourcePaths = pathsToBlast.collect(Collectors.toList());
		System.out.println(String.format("Start analysis of %d source paths", sourcePaths.size()));
		sourcePaths.forEach(System.out::println);

		final Launcher launcher = new Launcher();
		System.out.println(launcher.getEnvironment().getOutputType());

		sourcePaths.forEach(launcher::addInputResource);
		launcher.getFactory().getEnvironment().setPrettyPrinterCreator(() -> new SniperJavaPrettyPrinter(launcher.getFactory().getEnvironment()));
		launcher.getEnvironment().setComplianceLevel(10);
		final Path outputDir = Paths.get("spooned");
		launcher.setSourceOutputDirectory(outputDir.toFile());
		launcher.getEnvironment().setNoClasspath(true);
		launcher.getEnvironment().setOutputType(OutputType.COMPILATION_UNITS);

		return new Analyzed(launcher, outputDir, workspaceRoot);

	}

}
