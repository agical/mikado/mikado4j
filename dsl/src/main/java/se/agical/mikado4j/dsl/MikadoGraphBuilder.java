package se.agical.mikado4j.dsl;

public class MikadoGraphBuilder {

	public static <C> MikadoGraph<C> goal(String goalDescription) {
		return MikadoGraphBuilder.<C>goal(goalDescription, b -> b);
	}

	public static <C> MikadoGraph<C> goal(String goalDescription, PrereqDefiner<C, C> prereqs) {

		MikadoNodeBuilder<C, C> prereqBuilder = MikadoNodeBuilder.create(goalDescription);
		MikadoNode<C> node = prereqs.buildPrereq(prereqBuilder).finish();

		return new MikadoGraph<C>(node);
	}

}
