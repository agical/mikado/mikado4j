package example;

import se.agical.mikado4j.spoon.ConsoleTools;
import spoon.reflect.code.CtFieldAccess;
import spoon.reflect.declaration.CtField;

public class InvertCellFieldData {

	public final CtFieldAccess<?> cellFieldAccess;
	public final CtField<?> fieldDeclaration;

	public static InvertCellFieldData from(CtFieldAccess<?> field, CtField<?> oldFieldDeclaration) {
		return new InvertCellFieldData(field, oldFieldDeclaration);
	}

	public InvertCellFieldData(CtFieldAccess<?> cellFieldAccess, CtField<?> fieldDeclaration) {
		super();
		this.cellFieldAccess = cellFieldAccess;
		this.fieldDeclaration = fieldDeclaration;
	}

	public String describe() {
		return ConsoleTools.clickableLocation(cellFieldAccess);
	}

}
