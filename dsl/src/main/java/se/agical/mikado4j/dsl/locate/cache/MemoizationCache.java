package se.agical.mikado4j.dsl.locate.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import se.agical.mikado4j.dsl.locate.Locator;
import se.agical.mikado4j.dsl.locate.Many;

public class MemoizationCache<C, O> {
	private Lock lock = new ReentrantLock();
	private Map<Key<O>, Many<O>> cache = new HashMap<>();

	public Locator<C, O> wrap(KeyCreator<C, O> keyCreator, Locator<C, O> locate) {
		return c -> {
			Key<O> key = keyCreator.createKey(c);
			lock.lock();
			try {
				return cache.computeIfAbsent(key, k -> locate.locate(c));
			} finally {
				lock.unlock();
			}
		};
	}

}
