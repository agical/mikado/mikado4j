package se.agical.mikado4j.dsl.json;

import java.util.Arrays;

public class Node {
	public final String desc;
	public final int cardinality;
	public final boolean done;
	public final Node[] reqs;

	public Node() {
		this("");
	}

	public Node(String desc, Node... prereqs) {
		this(desc, 1, false, prereqs);
	}

	public Node(String desc, int cardinality, boolean done, Node... prereqs) {
		this.desc = desc;
		this.cardinality = cardinality;
		this.done = done;
		this.reqs = prereqs;
	}

	@Override
	public String toString() {
		return String.format("(([%s] %s [%d])) -> %s", done ? "v" : "-", desc, cardinality, Arrays.toString(reqs));
	}

	public static Node goal(String description, Node... prereqs) {
		return new Node(description, prereqs);
	}

	public static Node prereq(String description, Node... prereqs) {
		return new Node(description, prereqs);
	}

	/**
	 * How many instances of this node needs to be resolved?
	 * 
	 * @param number of instances that needs resolving
	 * @return A node with the specified cardinality
	 */
	public Node cardinality(int num) {
		return new Node(desc, num, done, reqs);
	}

	public Node notDone() {
		return new Node(desc, cardinality, false, reqs);
	}

	public Node done() {
		return new Node(desc, cardinality, true, reqs);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cardinality;
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		result = prime * result + (done ? 1231 : 1237);
		result = prime * result + Arrays.hashCode(reqs);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (cardinality != other.cardinality)
			return false;
		if (desc == null) {
			if (other.desc != null)
				return false;
		} else if (!desc.equals(other.desc))
			return false;
		if (done != other.done)
			return false;
		if (!Arrays.equals(reqs, other.reqs))
			return false;
		return true;
	}

}
