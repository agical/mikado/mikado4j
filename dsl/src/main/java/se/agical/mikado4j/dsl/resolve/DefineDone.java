package se.agical.mikado4j.dsl.resolve;

import se.agical.mikado4j.dsl.Prereqs;

public interface DefineDone<RC, CC> extends Prereqs<RC, CC> {
	Prereqs<RC, CC> updateDoneChecker(DoneChecker<CC> doneChecker);

	default Prereqs<RC, CC> done() {
		return updateDoneChecker(ccs -> true);
	}

	default Prereqs<RC, CC> notDone() {
		return updateDoneChecker(ccs -> false);
	}

	default Prereqs<RC, CC> doneWhenNoneLeft() {
		return updateDoneChecker(ccs -> ccs.size() == 0);
	}
}
