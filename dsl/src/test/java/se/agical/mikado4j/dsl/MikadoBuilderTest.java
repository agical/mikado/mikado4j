package se.agical.mikado4j.dsl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import se.agical.mikado4j.dsl.json.Node;
import se.agical.mikado4j.dsl.locate.Many;

class MikadoBuilderTest {

	@Test
	void buildAGraphWithASingleGoal() {
		MikadoGraph<?> mikadoGraph = MikadoGraphBuilder
				.goal("Mikado Goal");

		assertEquals(
				Node.goal("Mikado Goal"),
				mikadoGraph.getDom());

	}

	@Test
	void buildAGraphWithSingleLevelPrerequisites() {
		MikadoGraph<?> mikadoGraph = MikadoGraphBuilder
				.goal("Mikado Goal",
						b -> b
								.prereq("Req 1")
								.prereq("Req 2"));

		assertEquals(
				Node.goal("Mikado Goal",
						Node.prereq("Req 1"),
						Node.prereq("Req 2")),
				mikadoGraph.getDom());
	}

	@Test
	void buildAGraphWithMultiLevelPrerequisites() {
		MikadoGraph<?> mikadoGraph = MikadoGraphBuilder
				.goal("Mikado Goal",
						b -> b
								.prereq("Req 1", b2 -> b2
										.prereq("Req 1.1")));

		assertEquals(
				Node.goal("Mikado Goal",
						Node.prereq("Req 1",
								Node.prereq("Req 1.1"))),
				mikadoGraph.getDom());
	}

	@Nested
	class Cardinality {
		@Test
		void analyzeAnUnresolvedGoal() throws Exception {
			MikadoGraph<List<Integer>> mikadoGraph = MikadoGraphBuilder
					.<List<Integer>>goal("Delete even numbers", b -> b
							.<Integer>select(context -> context.stream()
									.filter(n -> n % 2 == 0)
									.collect(Many.collector())));

			assertEquals(
					Node.goal("Delete even numbers")
							.cardinality(5),
					mikadoGraph.analyze(Stream.iterate(1, l -> l + 1)
							.limit(10)
							.collect(Collectors.toList())));
		}

		@Test
		void analyzeAnUnresolvedGoalWithPrerequsite() throws Exception {
			MikadoGraph<List<Integer>> mikadoGraph = MikadoGraphBuilder
					.<List<Integer>>goal("Delete even numbers", b -> b
							.select(context -> () -> context.stream()
									.filter(n -> n % 2 == 0))
							.prereq("first this needs to be done"));

			assertEquals(
					Node.goal("Delete even numbers",
							Node.prereq("first this needs to be done")
									.cardinality(5))
							.cardinality(5),
					mikadoGraph.analyze(Stream.iterate(1, l -> l + 1)
							.limit(10)
							.collect(Collectors.toList())));
		}

		@Test
		void aPrerequsiteWithNoSelectedNodesHasACardinalytyOf0() throws Exception {
			MikadoGraph<List<Integer>> mikadoGraph = MikadoGraphBuilder
					.<List<Integer>>goal("Delete even numbers", b -> b
							.select(context -> context.stream()
									.filter(n -> n % 2 == 0)
									.collect(Many.collector()))
							.prereq("first this needs to be done", b2 -> b2
									.select(i -> Many.empty())));

			assertEquals(
					Node.goal("Delete even numbers",
							Node.prereq("first this needs to be done").cardinality(0))
							.cardinality(5),
					mikadoGraph.analyze(Stream.iterate(1, l -> l + 1)
							.limit(10)
							.collect(Collectors.toList())));
		}

		@Test
		void aPrerequsiteWithNoSelectedAbsoluteNodesHasACardinalytyOf0() throws Exception {
			MikadoGraph<List<Integer>> mikadoGraph = MikadoGraphBuilder
					.<List<Integer>>goal("Delete even numbers", b -> b
							.select(context -> () -> context.stream()
									.filter(n -> n % 2 == 0))
							.prereq("first this needs to be done", b2 -> b2
									.selectAbsolute(i -> Many.empty())));

			assertEquals(
					Node.goal("Delete even numbers",
							Node.prereq("first this needs to be done").cardinality(0))
							.cardinality(5),
					mikadoGraph.analyze(Stream.iterate(1, l -> l + 1)
							.limit(10)
							.collect(Collectors.toList())));
		}

	}

	@Nested
	class CheckingIfDone {

		@Test
		void explicitDone() throws Exception {
			MikadoGraph<List<Integer>> mikadoGraph = MikadoGraphBuilder
					.<List<Integer>>goal("Delete even numbers", b -> b
							.<Integer>select(context -> () -> context.stream()
									.filter(n -> n % 2 == 0))
							.done());

			assertEquals(
					Node.goal("Delete even numbers")
							.cardinality(5)
							.done(),
					mikadoGraph.analyze(Stream.iterate(1, l -> l + 1)
							.limit(10)
							.collect(Collectors.toList())));
		}

		@Test
		void doneWhenNoneLeft_SomeLeft() throws Exception {
			MikadoGraph<List<Integer>> mikadoGraph = MikadoGraphBuilder
					.<List<Integer>>goal("Delete even numbers", b -> b
							.<Integer>select(context -> context.stream()
									.filter(n -> n % 2 == 0)
									.collect(Many.collector()))
							.doneWhenNoneLeft());

			assertEquals(
					Node.goal("Delete even numbers")
							.cardinality(5)
							.notDone(),
					mikadoGraph.analyze(Stream.iterate(1, l -> l + 1)
							.limit(10)
							.collect(Collectors.toList())));
		}

		@Test
		void doneWhenNoneLeft_NoneLeft() throws Exception {
			MikadoGraph<List<Integer>> mikadoGraph = MikadoGraphBuilder
					.<List<Integer>>goal("Delete even numbers", b -> b
							.<Integer>select(context -> context.stream()
									.filter(n -> n % 2 == 0)
									.collect(Many.collector()))
							.doneWhenNoneLeft());

			assertEquals(
					Node.goal("Delete even numbers")
							.cardinality(0)
							.done(),
					mikadoGraph.analyze(Stream.iterate(1, l -> l + 1)
							.filter(n -> n % 2 == 1)
							.limit(10)
							.collect(Collectors.toList())));
		}

	}

	@Nested
	class SolvingNodes {

		@Test
		void doneWhenNoneLeft_NoneLeft() throws Exception {
			MikadoGraph<List<Integer>> mikadoGraph = MikadoGraphBuilder
					.<List<Integer>>goal("Delete even numbers", b -> b
							.<Integer>select(context -> context.stream()
									.filter(n -> n % 2 == 0)
									.collect(Many.collector()))
							.resolve(r -> r
									.name("divide even by two")
									.shortDescription(
											i -> Arrays.asList(String.format("divide by two %d/2=%d", i, i / 2))))
							.doneWhenNoneLeft());

			assertEquals(
					Node.goal("Delete even numbers")
							.cardinality(0)
							.done(),
					mikadoGraph.analyze(Stream.iterate(1, l -> l + 1)
							.filter(n -> n % 2 == 1)
							.limit(10)
							.collect(Collectors.toList())));
		}

	}

}
